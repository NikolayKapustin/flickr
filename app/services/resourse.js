/**
 * Created by nik on 03.08.16.
 */
angular.module('app.services')
.factory('resource', ['$resource', function ($resource) {
  const url = 'https://api.flickr.com/services/rest/';
  return $resource(url, {}, {});
}]);