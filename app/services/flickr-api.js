/**
 * Created by nik on 03.08.16.
 */
const settings = require('../settings');

angular.module('app.services')
  .service('flickr', ['resource', 'utils', flickr]);

function flickr(resource, utils) {
  const apiKey = settings.apiKey;
  const apiSecret = settings.apiSecret;
  var token = utils.getUserInfo().token;
  var nsid = utils.getUserInfo().nsid;

  this.getToken = function () {
    return token;
  };
  this.getNsid = function () {
    return nsid;
  };
  this.getApiKey = function () {
    return apiKey;
  };
  this.saveUserInfo = function (newToken, newNsid) {
    token = newToken;
    nsid = newNsid;
    utils.saveUserInfo(token, nsid);
  };

  /**
   * return api_sig
   * @param {Object} params
   */
  this.apiSig = function(params) {
    const api_key  = 'api_key';
    var string = apiSecret;
    var arr = [];

    for(let i in params) {
      if (params.hasOwnProperty(i)) {
        arr.push(i+params[i]);
      }
    }
    arr.sort();
    string+=arr.join('');
    return md5(string);
  };


  /**
   * Получить токен после авторизации
   * @param {String} frob
   * @param {Function} cb
   */
  this.getNewToken = function (frob, cb) {
    var params = {
      method: 'flickr.auth.getToken',
      api_key: apiKey,
      frob: frob,
      format: 'json',
      nojsoncallback: 1
    };
    params.api_sig = this.apiSig(params);

    return resource.get(params, cb);
  };

  /**
   * получить список альбомов
   * @param {Function} cb
   * @returns {*}
   */
  this.getAlbums = function (cb) {
    var params = {
      method: 'flickr.photosets.getList',
      api_key: apiKey,
      primary_photo_extras: 'url_s',
      auth_token: token,
      format: 'json',
      nojsoncallback: 1
    };
    params.api_sig = this.apiSig(params);
    return resource.get(params, cb);
  };


  /**
   * Получить фотографии альбома
   * @param {String} id
   * @param {Function} cb
   * @returns {*}
   */
  this.getPhotos = function (id, cb) {
    var params = {
      method: 'flickr.photosets.getPhotos',
      photoset_id: id,
      api_key: apiKey,
      extras: 'url_s,url_o',
      auth_token: token,
      format: 'json',
      nojsoncallback: 1
    };
    params.api_sig = this.apiSig(params);
    return resource.get(params, cb);
  };

  /**
   * Добавляет фото в альбом
   * @param {String} photoSetId
   * @param {String} photoId
   * @param {Function} cb
   * @returns {*}
   */
  this.addPhotoToAlbum = function (photoSetId, photoId, cb) {
    var params = {
      method: 'flickr.photosets.addPhoto',
      photoset_id: photoSetId,
      api_key: apiKey,
      photo_id: photoId,
      auth_token: token,
      format: 'json',
      nojsoncallback: 1
    };
    params.api_sig = this.apiSig(params);
    return resource.get(params, cb);
  }
}