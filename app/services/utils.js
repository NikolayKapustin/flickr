/**
 * Created by nik on 03.08.16.
 */
const settings = require('../settings');
const tokenName = 'flickr-token';
const nsidName = 'nsid';

angular.module('app.services')
  .factory('utils', utils);

function utils() {
  return {
    /**
     * формирует ссылку с параметрами из объекта
     * @param {Object} params
     * @param {string} link
     */
    makeParamsString: function(params, link) {
      link = link || '';
      return link+'?'+$.param(params);
    },

    /**
     * Сохраняет информацию о пользователе
     * @param {String} token
     * @param {String} nsid
     */
    saveUserInfo: function(token, nsid) {
      localStorage.setItem(tokenName, token);
      localStorage.setItem(nsidName, nsid);
    },
    
    /**
     * возвращает информацию о пользователе
     */
    getUserInfo: function () {
      return {
        token: localStorage.getItem(tokenName),
        nsid: localStorage.getItem(nsidName)
      }
    }
  }
}