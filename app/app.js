/**
 * Created by nik on 02.08.16.
 */
require('./router');
require('./components');
require('./services');
require('./directives');

angular.module('app', [
  'app.router',
  'app.services',
  'app.components',
  'app.directives'
]);