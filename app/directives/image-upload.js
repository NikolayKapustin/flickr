/**
 * Created by nik on 04.08.16.
 */
angular.module('app.directives')
  .directive('imageUploader', ['flickr', dir]);

function dir(flickr) {
  return {
    restrict: 'E',
    scope: {
      getPhotoId: '&'
    },
    template: '<form method="post" enctype="multipart/form-data" target="iframe" action="https://up.flickr.com/services/upload/"><input name="photo" type="file"></form><div class="content"><div class="file-progress"></div><span class="glyphicon glyphicon-plus"></span></div>',
    link: function (scope, el) {
      var form = el.find('form');
      var input = el.find('input');
      var content = el.find('.content');
      var progressBar = el.find('.file-progress');

      content.on('click', () => input.trigger('click'));

      form.on('change', e => {
        var params = {
          api_key: flickr.getApiKey(),
          auth_token: flickr.getToken(),
          format: 'json'
        };

        params.api_sig = flickr.apiSig(params);

        for (let i in params) {
          if (params.hasOwnProperty(i)) {
            form.append($("<input>")
              .attr("type", "hidden")
              .attr("name", i)
              .val(params[i]));
          }
        }

        form.ajaxSubmit({
            uploadProgress:function (e) {
              var t = e.total;
              var p = e.loaded;
              var percent = 100*p/t;
              setProgress(percent);
            },
            success: function (e) {
              setProgress(0);
              var photoId = $(e).find('photoid').text();
              if (!photoId) return false;
              scope.getPhotoId({photoId: photoId});
            }
          }
        );
      });
      
      function setProgress(percent) {
        percent = percent.toFixed(0);
        progressBar.css({'height':percent+'%'});
      }
    }
  }
}