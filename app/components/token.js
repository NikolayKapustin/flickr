/**
 * Created by nik on 03.08.16.
 */
angular.module('app.components')
  .component('token', {
    template:'<div></div>',
    controller: ['$stateParams', '$state', 'flickr', ctrl]
  });

function ctrl($stateParams, $state, flickr) {
  var frob = $stateParams.frob;

  flickr.getNewToken(frob, function (res) {
    if (res.stat == 'fail') {
      console.log(res.message);
      return $state.go('auth');
    }
    
    var token = res.auth.token._content;
    var userId = res.auth.user.nsid;
    flickr.saveUserInfo(token, userId);
    $state.go('albums');
  });
}