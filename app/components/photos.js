/**
 * Created by nik on 04.08.16.
 */
angular.module('app.components')
  .component('photos', {
    templateUrl: '/templates/photos.html',
    controller: ['$stateParams', '$state', 'flickr', ctrl]
  });

function ctrl($stateParams, $state, flickr) {
  var albumId = $stateParams.id;
  flickr.getPhotos(albumId, data => {
    this.photos = data.photoset.photo;
  });

  this.showPhoto = function (url, width) {
    this.review = url;
    this.reviewWidth = width;
  };
  
  this.hidePhoto = function () {
    this.review = null;
    this.reviewWidth = null;
  };
  
  this.getPhotoId = function (photoId) {
    flickr.addPhotoToAlbum(albumId, photoId, res => {
      if (res.stat == 'ok') {
        $state.reload();
      }
    });
  }
}