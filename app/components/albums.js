/**
 * Created by nik on 03.08.16.
 */
angular.module('app.components')
  .component('albums', {
    templateUrl: '/templates/albums.html',
    controller: ['flickr', ctrl]
  });

function ctrl (flickr) {
  flickr.getAlbums(data => {
    this.photosets = data.photosets.photoset;
  });
}