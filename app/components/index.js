/**
 * Created by nik on 03.08.16.
 */
angular.module('app.components', []);
require('./auth');
require('./token');
require('./albums');
require('./photos');