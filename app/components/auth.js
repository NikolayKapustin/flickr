/**
 * Created by nik on 03.08.16.
 */
angular.module('app.components')
  .component('auth', {
    templateUrl: '/templates/auth.html',
    controller: ['utils', 'flickr', '$state', ctrl]
  });

function ctrl(utils, flickr, $state) {
  if (flickr.getToken()) {
    $state.go('albums');
  }

  this.authLink = function () {
    const link = 'http://flickr.com/services/auth/';
    const perms = 'delete';

    var params = {
      api_key: flickr.getApiKey(),
      perms: perms
    };

    params.api_sig = flickr.apiSig(params);

    return utils.makeParamsString(params, link);
  }
} 