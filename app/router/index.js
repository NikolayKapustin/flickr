/**
 * Created by nik on 03.08.16.
 */
angular.module('app.router', ['ui.router'])

.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider
    .state('auth', {
      url: "/auth",
      template: "<auth></auth>"
    })
    .state('token', {
      url: '/getToken?frob',
      template: '<token></token>'
    })
    .state('albums', {
      url: '/albums',
      template: '<albums></albums>'
    })
    .state('photos', {
      url: '/albums/:id',
      template: '<photos></photos>'
    });

  $urlRouterProvider.otherwise("/auth");
});