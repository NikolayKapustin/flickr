/**
 * Created by nik on 02.08.16.
 */
const gulp = require('gulp');
const browserify = require('gulp-browserify');
const gp_uglify = require('gulp-uglify');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const watch = require('gulp-watch');
const livereload = require('gulp-livereload');
const babel = require('gulp-babel');

gulp.task('default', ['vendors', 'js', 'sass']);

gulp.task('vendors', function() {
  gulp.src('./app/vendors.js')
    .pipe(browserify({
      shim: {
        jquery : {
          path: './node_modules/jquery/dist/jquery.js',
          exports: '$'
        },
        angular : {
          path: './node_modules/angular/angular.js',
          exports: 'ko'
        },
        'js-md5' : {
          path: './node_modules/js-md5/src/md5.js',
          exports: 'md5'
        }
      }
    }))
    .pipe(gp_uglify())
    .pipe(gulp.dest('./build/js'))
});

gulp.task('js', function() {
  gulp.src('./app/app.js')
    .pipe(browserify({
      debug : true
    }))
    .on('error', function(err){
      console.log(err.message);
      this.end();
    })
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(gulp.dest('./build/js'))
});

gulp.task('sass', function() {
  gulp.src('./sass/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .on('error', function(err){
      console.log(err.message);
      this.end();
    })
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./build/css'))
    .pipe(livereload());
});

gulp.task('watch', function () {
  livereload.listen({start: true});
  gulp.watch('./sass/**/*.scss', ['sass']);
  gulp.watch('./app/**/*.js', ['js']);
});